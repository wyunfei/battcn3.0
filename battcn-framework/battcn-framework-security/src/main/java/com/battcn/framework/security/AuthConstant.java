package com.battcn.framework.security;

/**
 * @author Levin
 * @since 2018/4/9 0009
 */
public class AuthConstant {

    /**
     * Token存放的请求头
     */
    public static final String AUTHORIZATION = "X-Authorization";


}
