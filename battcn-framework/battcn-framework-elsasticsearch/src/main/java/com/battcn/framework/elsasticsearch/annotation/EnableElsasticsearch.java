package com.battcn.framework.elsasticsearch.annotation;

import java.lang.annotation.*;

/**
 * 开启JWT Token
 *
 * @author Levin
 * @since 2018-01-15
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface EnableElsasticsearch {


}
